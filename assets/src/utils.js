const animateText = (beginText, ref, sec=3000) => {
    let i = 0;

    let intervalRelease = setInterval(() => {
        if (i == beginText.length) {
            clearInterval(intervalRelease);
        }

        ref(beginText.slice(0, i));
        i++;
    }, sec / (beginText.length));

    return intervalRelease;
};

module.exports = animateText;