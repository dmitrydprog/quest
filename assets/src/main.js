import Vue from 'vue';
import Quest from './components/Quest.vue';

new Vue({
    el: '#app',
    components: {
        quest: Quest
    },
    data: {
        research: {}
    },
    delimiters: ['[[', ']]']
});