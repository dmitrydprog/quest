from .settings import *

DEBUG = False
ALLOWED_HOSTS = ['quest.dissw.ru']
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'open_door_quest',
        'USER': 'quest_user',
        'PASSWORD': 'super_quest_password',
        'HOST': 'postgres',
        'PORT': '',
    }
}