from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import TemplateView

from main.urls import router
from main.views import FirstQuest

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^first/', FirstQuest.as_view()),
    url(r'^$', TemplateView.as_view(template_name='index.html')),
]
