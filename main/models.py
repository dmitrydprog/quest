from django.db.models.base import Model
from django.db.models.fields import TextField, BooleanField, IntegerField
from django.db.models.fields.related import ForeignKey

type = (
    (0, 'Титульный'),
    (1, 'Первый'),
    (2, 'Вопрос с кодом'),
    (3, 'Вопрос с ответом'),
    (4, 'Последний'),
)


class Quest(Model):
    description = TextField(verbose_name='Описание')
    answer = TextField(verbose_name='Ответ')
    next = ForeignKey('self', null=True, blank=True)
    code = TextField(verbose_name='Код', null=True, blank=True, default=None)
    
    type = IntegerField(verbose_name='Тип вопроса', choices=type, default=2)
    
    @property
    def previous(self):
        try:
            obj = Quest.objects.get(next=self)
        except:
            obj = None
        
        return obj
    
    def __str__(self):
        return self.description
    
    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'
