from rest_framework import serializers
from rest_framework.serializers import ModelSerializer, HyperlinkedModelSerializer

from main.models import Quest


class RecursiveField(serializers.Serializer):
    def to_native(self, value):
        return self.parent.to_native(value)


class QuestSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Quest
        fields = '__all__'
