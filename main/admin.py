from django.contrib import admin
from django.contrib.admin.options import ModelAdmin

from main.models import Quest


@admin.register(Quest)
class QuestAdmin(ModelAdmin):
    pass
