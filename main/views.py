from django.http.response import JsonResponse
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from main.models import Quest
from main.serializers import QuestSerializer


class QuestViewSet(ModelViewSet):
    queryset = Quest.objects.all()
    serializer_class = QuestSerializer


class FirstQuest(APIView):
    def get(self, request):
        try:
            obj = Quest.objects.get(type=1)
        except:
            obj = None

        obj_json = QuestSerializer(obj, context={
            'request': request
        })

        return JsonResponse(data=obj_json.data)