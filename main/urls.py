from rest_framework.routers import DefaultRouter

from main.views import QuestViewSet, FirstQuest

router = DefaultRouter()
router.register(r'quests', QuestViewSet)
